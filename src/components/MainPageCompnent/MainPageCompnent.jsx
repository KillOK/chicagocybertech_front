import React from 'react'
import CompanyDescriptionComponent from './CompanyDescriptionComponent'
import ServicesDescriptionComponent from './ServicesDescriptionComponent'
import OurMissionComponent from './OurMissionComponent'
import {Button} from 'react-bootstrap'
import {withRouter} from 'react-router';



class MainPageCompnent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        let scrollToTop = () =>{
            window.scrollTo({
              top: 0, 
              //   behavior: 'smooth'
              /* you can also use 'auto' behaviour
                 in place of 'smooth' */
            });
          };
        let goToApplicationForm = (e)=>{
            e.preventDefault();
            // console.log(this.props);
            scrollToTop();
            this.props.history.push("/application") 
        }

        return (
            <div>
                <div className="pageBaseComponent mainPageFirstLvl w-100 mx-1 px-5 ">
                <img src="/MainPage/ChicagoSkyline.jpg" alt="Chicago.logo" width="100%" height="80%"/>
                <br/>
                <br/>
                </div>
                <div className="mainPageSecondLvl  w-100 mx-1 px-5 ">
                <hr className="solid my-0"></hr>
                <br/>
                <CompanyDescriptionComponent/>
                <br/>
                <hr className="solid my-0"></hr>
                {/* <hr className="solid"></hr> */}
                </div>
                <div className="mainPageThirdLvl w-100 mx-1 px-5 ">
                <br/>
                <ServicesDescriptionComponent/>
                <br/>
                </div>
                <div className="mainPageSecondLvl  w-100 mx-1 px-5 textCenter">
                <hr className="solid my-0"></hr>
                    <br/>
                    <h2>Did you find the service you need? Click apply  <Button variant="danger" size="md" onClick={goToApplicationForm}>Apply</Button></h2>
                    <br/>
                <hr className="solid my-0"></hr>
                </div>
                <div className="mainPageFourthLvl  w-100 mx-1 px-5 ">
                <br/>
                <OurMissionComponent/>
                <br/>
                </div>
            </div>
        );
    }
}

export default (withRouter(MainPageCompnent));