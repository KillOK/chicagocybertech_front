import React from 'react';
import {Carousel} from "react-bootstrap";

class CompanyDescriptionComponent extends React.Component {
    constructor(props) {
        super(props)
        //776px
        this.state = { matches: window.matchMedia("(min-width: 776px)").matches };
      }
    state = {  }

    componentDidMount() {
        const handler = e => this.setState({matches: e.matches});
        window.matchMedia("(min-width: 776px)").addEventListener('change', handler);
    }
    
    componentWillUnmount() {
        const handler = e => this.setState({matches: e.matches});
        window.matchMedia("(min-width: 776px)").removeEventListener('change', handler);
      }
    render() { 
        return ( 
            <div className="containerm">
                <div className="row">
                    <div className="col-lg-5 mt-4 textCenter">
                        <br/>
                        <h3>ChicagoCyberTech LLC</h3>
                        <br/>
                                <p className='textJustify'>We provide a full suite of managed information technology IT services and solutions for businesses starting from design and development of your applications to delivery and support.</p>
                    </div>
                    <div className="col col-lg-7 " >
                        <Carousel>
                            <Carousel.Item interval={3000}>
                                <img
                                className="d-block mainPageImgOverlap"
                                src="/MainPage/CompanyDesc/DesignOpacOptim.png"
                                alt="Design"
                                height={this.state.matches?"320px":"200px"}
                                />
                            </Carousel.Item>
                            <Carousel.Item interval={3000}>
                                <img
                                className="d-block  mainPageImgOverlap"
                                src="/MainPage/CompanyDesc/DevelopmentOpacOptim.png"
                                alt="Development"
                                height={this.state.matches?"320px":"200px"}
                                
                                />
                            </Carousel.Item>
                            <Carousel.Item interval={3000}>
                                <img
                                className="d-block mainPageImgOverlap"
                                src="/MainPage/CompanyDesc/ServersOpacOptimized.png"
                                alt="DevOps"
                                height={this.state.matches?"320px":"200px"}
                                
                                />
                            </Carousel.Item>
                        </Carousel>
                        </div>
                </div>
            </div>
         );
    }
}
 
export default CompanyDescriptionComponent;