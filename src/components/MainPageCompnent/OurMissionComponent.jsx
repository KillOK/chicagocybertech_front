import React from 'react';
import {Carousel} from "react-bootstrap";
import '../../../src/styles/style.css';

class OurMissionComponent extends React.Component {
    constructor(props) {
        super(props)
        //776px
        this.state = { matches: window.matchMedia("(min-width: 776px)").matches };
      }
    state = {  }

    componentDidMount() {
        const handler = e => this.setState({matches: e.matches});
        window.matchMedia("(min-width: 776px)").addEventListener('change', handler);
    }
    
    componentWillUnmount() {
        const handler = e => this.setState({matches: e.matches});
        window.matchMedia("(min-width: 776px)").removeEventListener('change', handler);
      }

    render() { 
        return ( 
            <div >
                {/* {this.state.matches && (<h1>Big Screen</h1>)}
                {!this.state.matches && (<h3>Small Screen</h3>)}   */}
                    <h2 className='textCenter'>
                        Our Mission
                    </h2>
                <div className={this.state.matches?'textJustify ourMissionDescWide':"textJustify ourMissionDesc"}>
                    <p>
                    Our mission is to find and connect smart and creative people which will build applications able to satisfy all needs of your business.
                    </p>
                </div>
                <Carousel>
                            <Carousel.Item interval={3000}>
                                <img
                                className={this.state.matches?"d-block mainPageImgOverlap ":"d-block mainPageImgOverlap "}
                                src="/MainPage/OurMission/Training.png"
                                alt="Training"
                               height={this.state.matches?"320px":"200px"}
                                />
                            </Carousel.Item>
                            <Carousel.Item interval={3000}>
                                <img
                                className="d-block mx-auto mainPageImgOverlap"
                                src="/MainPage/OurMission/ConnectPeople.png"
                                alt="ConnectPeople"
                               height={this.state.matches?"320px":"200px"}
                                />
                            </Carousel.Item>
                            <Carousel.Item interval={3000}>
                                <img
                                // className="d-block mx-auto mainPageImgOverlap"
                                className={this.state.matches?"d-block mainPageImgOverlap ":"d-block mainPageImgOverlap "}
                                src="/MainPage/OurMission/TeamWork.png"
                                alt="TeamWork"
                               height={this.state.matches?"320px":"200px"}
                                />
                            </Carousel.Item>
                            <Carousel.Item interval={3000}>
                                <img
                                className="d-block mx-auto mainPageImgOverlap"
                                src="/MainPage/OurMission/BoostSales.png"
                                alt="BoostSales"
                               height={this.state.matches?"320px":"200px"}
                                />
                            </Carousel.Item>
                        </Carousel>
            </div>
         );
    }
}
 
export default OurMissionComponent;