import React from 'react';

class ServicesDescriptionComponent extends React.Component {
    // constructor(props) {
    //     super(props);
    // }
    state = {  }
    render() { 
        return ( 
            <div >
                <h2 className='textCenter'>Our Services</h2>
                <div className="container">
                    {/* //////////Design////////////// */}
                    <div className="row">
                        <div className="col-md-6">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurServices/DesignOpacIcon.png" alt="UX/UI" className='serviceDescriptionIcon'/>
                            </div>
                            <div className="col-sm-8 textJustify mt-2" >
                                <p>We Design your application according to the best UI/UX practices</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-md-6">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurServices/frontend.png" alt="UX/UI" className='serviceDescriptionIcon'/>
                            </div>
                                <div className="col-sm-8 textJustify mt-2" >
                                    <p>We implement your application with the best modern FrontEnd frameworks like React JS, Vue JS, or Next JS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    {/* ///////////RDS///////////// */}
                    <div className="row">
                        <div className="col-md-6">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurServices/DB.png" alt="RDBS" className='serviceDescriptionIcon pt-1'/>
                            </div>
                            <div className="col-sm-8 textJustify mt-2" >
                                <p>We store your data securely with modern RDB and NoSQL Data Bases</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-md-6">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurServices/backend.png" alt="BackEnd" className='serviceDescriptionIcon'/>
                            </div>
                            <div className="col-sm-8 textJustify mt-2" >
                                <p>We transfer and process your data securely with modern server-side solutions like Express JS or Spring Boot</p>
                            </div>
                            </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    {/* ////////////DevOps//////////// */}
                    <div className="row">
                        <div className="col-md-6">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurServices/DevOps.png" alt="DevOps" className='serviceDescriptionIcon px-3 mt-3'/>
                            </div>
                            <div className="col-sm-8 textJustify mt-2" >
                                <p>Our DevOps team provide Continuous Integration and Continuous Delivery for your applications</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-md-6">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurServices/supportCopy.png" alt="Tech Support" className='serviceDescriptionIcon' />
                            </div>
                            <div className="col-sm-8 textJustify mt-2" >
                                <p>We provide Technical Support and application maintenance 24/7</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                </div>
            </div>
         );
    }
}
 
export default ServicesDescriptionComponent;