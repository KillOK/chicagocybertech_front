import React from 'react';
import {Button} from 'react-bootstrap';
import {withRouter} from 'react-router';

class ServicesListComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    
    
    
    render() {
        let scrollToTop = () =>{
            window.scrollTo({
              top: 0, 
            });
          };

        let goToApplicationForm = (e)=>{
            e.preventDefault();
            scrollToTop();
            this.props.history.push("/application") ;
        }

        return (
            
        <div>
            <div className="ServicesPageBgImg pageBaseComponent mainPageFirstLvl w-100 mx-1 px-5 servicePageCaptionText ">
                <div className='w-100 h-100 servicePageCaptionText'>
                    <h1 className='servicePageCaptionTextPosition'>CHICAGOCYBERTECH</h1>
                    <h3 className='servicePageSubTextPosition'>Best solutions for your business</h3>
                </div>
            </div>
            <div className="mainPageSecondLvl  w-100 mx-1 px-5 textCenter bg-dark servicePageCaptionText">
                <hr className="solid my-0"></hr>
                    <br/>
                    <h2>OUR SERVICES</h2>
                    <br/>
                <hr className="solid my-0"></hr>
            </div>
            <div className="mainPageThirdLvl  w-100 mx-1 servicePageCaptionText">
                <div className="container">
                    {/* //////////Design////////////// */}
                    <div className="row">
                        <div className="col-md-6 servicesChessDarkGray">
                            <div className="row">
                                <div className="col-sm-4 px-0 mt-2">
                                    <img className='my-auto w-100 serviceDescriptionIcon' src="/MainPage/OurServices/DesignOpacIcon.png" alt="UX/UI" />
                                </div>
                                <div className="col-sm-8 textJustify my-auto" >
                                    <p>We Design cross-browser responsive WEB applications according to the best UI/UX practices</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 servicesChessLightGray">
                            <div className="row">
                                <div className="col-sm-4 px-0 mt-2">
                                    <img className='my-auto w-100 serviceDescriptionIcon' src="/MainPage/OurServices/frontend.png" alt="UX/UI" />
                                </div>
                                <div className="col-sm-8 textJustify my-auto" >
                                    <p>We implement your application with the best modern FrontEnd frameworks like React JS, Vue JS, or Next JS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    {/* ///////////RDS///////////// */}
                    <div className="row">
                        <div className="col-md-6 servicesChessWhite">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img className='my-auto w-100 serviceDescriptionIcon pt-3 pb-2' src="/MainPage/OurServices/DB.png" alt="RDBS" />
                            </div>
                            <div className="col-sm-8 textJustify my-auto" >
                                <p>We store your data securely with modern RDB and NoSQL Data Bases</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-md-6 servicesChessDarkGray">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img className='my-auto w-100 serviceDescriptionIcon' src="/MainPage/OurServices/backend.png" alt="BackEnd" />
                            </div>
                            <div className="col-sm-8 textJustify my-auto" >
                                <p>We transfer and process your data securely with modern server-side solutions like Express JS or Spring Boot</p>
                            </div>
                            </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    {/* ////////////DevOps//////////// */}
                    <div className="row">
                        <div className="col-md-6 servicesChessLightGray">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img className='my-auto w-100 serviceDescriptionIcon pl-3 pt-3 pb-3' src="/MainPage/OurServices/DevOps.png" alt="DevOps"/>
                            </div>
                            <div className="col-sm-8 textJustify my-auto" >
                                <p>Our DevOps team provide Continuous Integration and Continuous Delivery for your applications</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-md-6 servicesChessWhite">
                            <div className="row">
                                <div className="col-sm-4 px-0 mt-2">
                                    <img className='my-auto w-100 serviceDescriptionIcon' src="/MainPage/OurServices/supportCopy.png" alt="Tech Support" />
                                </div>
                                <div className="col-sm-8 textJustify my-auto" >
                                    <p>We provide Technical Support and application maintenance 24/7 </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    <div className="row">
                        <div className="col-md-6 servicesChessDarkGray">
                            <div className="row">
                                <div className="col-sm-4 px-0 mt-2">
                                    <img className='my-auto w-100 serviceDescriptionIcon' src="/MainPage/OurServices/Mobile.png" alt="UX/UI" />
                                </div>
                                <div className="col-sm-8 textJustify my-auto" >
                                    <p>We design cross-platform mobile solutions which support most modern devices from smartwatches and smartphones to tablets etc.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 servicesChessLightGray">
                            <div className="row">
                                <div className="col-sm-4 px-0 mt-2">
                                    <img className='my-auto w-100 serviceDescriptionIcon' src="/MainPage/OurServices/Chatbot.png" alt="Chatbots" />
                                </div>
                                <div className="col-sm-8 textJustify my-auto" >
                                    <p>Our AI-powered assistants understand customers in context to provide fast, consistent, and accurate answers across any application, device, or channel. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="mainPageSecondLvl  w-100 mx-1 px-5 textCenter bg-dark servicePageCaptionText">
                <hr className="solid my-0"></hr>
                    <br/>
                    <h2>Did you find the service you need? Click apply  <Button variant="danger" size="md" onClick={goToApplicationForm}>Apply</Button></h2>
                    <br/>
                <hr className="solid my-0"></hr>
            </div>
            <div className="mainPageFourthLvl servicePageCaptionText servicesChessDarkGray w-100 mx-1 px-5 ">
                {/* ///////////////////////////////////////////////////////////////////////// */}
                
                <h2 className='servicePageCaptionText textCenter my-3'>PROJECTS WHICH HELP US</h2>
                <div className="container">
                    <div className="row">
                        
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://www.atlassian.com/' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/Atlassian.png" alt="Atlassian" />
                                </a></div>
                        </div>
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://getbootstrap.com/' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/Bootstrap.png" alt="Bootstrap" />
                            </a></div>
                        </div>
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://ru.freepik.com' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/freepik.jpg" alt="freepik" />
                            </a></div>
                        </div>
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://www.gimp.org/' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/Gimp.jpg" alt="Gimp" />
                            </a></div>
                        </div>
                        
                    </div>
                    {/* ////////////////////////////////////////////////////////////////////////// */}
                    
                    <div className="row">
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://linuxmint.com/' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/Mint.png" alt="Linux" />
                            </a></div>
                        </div>
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://www.postgresql.org/' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/PG.png" alt="postgresql" />
                            </a></div>
                        </div>
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://rasa.com/' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/RASA.jpg" alt="RASA" />
                            </a></div>
                        </div>
                        
                        
                        <div className="col-md-3 my-1 px-1">
                            <div className='servicePagePartnersImg w-100'><a href='https://grammarly.com/' target="_blank" rel="noopener noreferrer">
                                <img className='my-auto w-100' src="/ServicesPage/Partners/Grammarly.jpg" alt="Grammarly" />
                            </a></div>
                        </div>
                    </div>
                    <br/>
                </div>
                {/* ///////////////////////////////////////////////////////////////////////// */}
                    
                    
            </div>
        </div>
        );
    }
}

export default (withRouter(ServicesListComponent));