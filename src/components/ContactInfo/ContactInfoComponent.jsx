import React from 'react';

class ContactInfoComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = { matches: false};
      }
    state = {  }

render() {
    
    return (
    <div>
        <div className="ContactPageBgImg pageBaseComponent mainPageFirstLvl w-100 mx-1 px-5  ">
            <div className='w-100 h-100 servicePageCaptionText'>
                <h1 className='servicePageCaptionTextPosition'>CONTACT INFORMATION</h1>
            </div>
        </div>
        <div className="mainPageSecondLvl  w-100 mx-1 px-5 textCenter bg-dark servicePageCaptionText">
            <hr className="solid my-0"></hr>
                <br/>
                <h2>CONTACT US</h2>
                <br/>
            <hr className="solid my-0"></hr>
        </div>
        <div className="mainPageFourthLvl  w-100 mx-1 px-5 ">
            <hr className="solid my-0"></hr>
            <br/>
            <div className="container">
                
                    {/* //////////Upwork////////////// */}
                    <div className="row">
                        <div className="col-md-12">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurContact/upwork.png" alt="upwork" className='serviceDescriptionIcon'/>
                            </div>
                            <div className="col-sm-8 textCenter my-auto" >
                            <p>Our <a href='https://www.upwork.com/' target="_blank" rel="noopener noreferrer">Upwork</a> profile</p>
                            </div>
                        
                        </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    {/* ///////////Linkedin///////////// */}
                <div className="row">
                        <div className="col-md-12">
                        <div className="row">
                        {this.state.matches?
                            <div className="col-sm-8 textCenter my-auto " >
                                <p>Our <a href='https://www.linkedin.com/company/chicagocybertech-llc' target="_blank" rel="noopener noreferrer">Linkedin</a> profile</p>
                            </div>
                        :
                            ""
                        }
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurContact/linkedin.png" alt="linkedin" className='serviceDescriptionIcon pt-1'/>
                            </div>
                        {!this.state.matches?
                            <div className="col-sm-8 textCenter my-auto " >
                                <p>Our <a href='https://www.linkedin.com/company/chicagocybertech-llc' target="_blank" rel="noopener noreferrer">Linkedin</a> profile</p>
                            </div>
                        :
                            ""
                        }
                        </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    
                    {/* ////////////phone//////////// */}
                    <div className="row">
                        <div className="col-md-12">
                        <div className="row">
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurContact/email.png" alt="email" className='serviceDescriptionIcon '/>
                            </div>
                            <div className="col-sm-8 textCenter my-auto " >
                                <p>Our E-mail<br/>apply@chicagocybertech.com</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    {/* ///////////////////////////////////////////////////// */}
                    {/* ///////////e-mail///////////// */}
                    <div className="row">
                        <div className="col-md-12">
                        <div className="row">
                        {this.state.matches?
                            <div className="col-sm-8 textCenter my-auto " >
                                <p>Contact us by phone<br/>(331) 642-1137</p>
                            </div>
                        :
                            ""
                        }
                            <div className="col-sm-4 px-0 mt-2">
                                <img src="/MainPage/OurContact/phone.png" alt="phone" className='serviceDescriptionIcon pt-1'/>
                            </div>
                        {!this.state.matches?
                            <div className="col-sm-8 textCenter my-auto " >
                                <p>Contact us by phone<br/>(331) 642-1137</p>
                            </div>
                        :
                            ""
                        }
                        </div>
                        </div>
                    </div>
                    {/* //////////////////////// */}
                    </div>
            
            {/* ///////////////////////////////////////////////////////////// */}
            <br/>
            <hr className="solid my-0"></hr>
            </div>
        <br/>
        
    </div>
    );
}
}
 
export default ContactInfoComponent;