import React from 'react';
import {Button, Accordion, Card} from 'react-bootstrap';
import {Modal} from 'react-bootstrap';
let MyVerticallyCenteredModal=(props) =>{
    console.log(props)
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter" >
            {props.title?props.title:"Thank you for your interest in our company"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>To apply for this position...</h4>
          <p>
            Please send us your updated resume to our email apply@chicagocybertech.com<br/>
            Please include job title or position id
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }





class CareersListComponent extends React.Component {
    
        state = {
            modalShow:false,
            jobTitle:''
        };

    // modalShow = this.state.modalShow;
    // jobTitle = '';
    
    applyJob = (jobTitle)=>{
        this.setState(
            {
                ...this.state,
                jobTitle:jobTitle,
                modalShow:true
            }
        )
    }
    setModalShow = (val)=>{
        this.setState(
            {
                ...this.state,
                modalShow:val
            }
        )
    }
    
   
    render() {
        

        // let mockjobsJSON = [
        //     {
        //         jobTitle:"Java Developer",
        //         location:"Chicago IL",
        //         salary:"Non payed",
        //         jobType:"fullTime/partTime/contract/internship/training",
        //         jobDescription:`
        //         In this role, you will apply Agile and SDLC Quality Assurance (QA) methodology and standards to verify and validate products meet specifications and customer acceptance criteria. 
        //         You will create and execute enterprise, web-based, services and client server applications test plans and cases and log defects. You will demonstrate strong analytical problem solving, communication skills, and attention to detail by extensively documenting test procedures, expected results, and defects.`,
        //         responsibilities:`
        //         Create test cases, plans, and scripts based on story defined acceptance criteria.
        //         Identify, document, and report bugs, errors, and other issues with software applications.
        //         Develop, verify, and manage change requests for each component of a project solution.
        //         Analyze and identify all issues in requirements and design and assist to provide resolution.
        //         Prepare solid test cases that can be automated, executed, and logged.
        //         Development and execution of automated test scripts using JMeter, SQL and Selenium Webdriver.
        //         Creating API testing framework and perform API testing.
        //         Raise defects in the defect tracking system.
        //         Design traceability matrix to match the test scripts with the functional design document.
        //         Use complex SQL queries for testing backend data.`,
        //         qualifications:`
        //         Relevant degree preferred in Computer Science.
        //         Knowledge of Object Oriented Programming (C#, Java), 
        //         AGILE stories sizing, writing SQL Statements required.
        //         2 or more year’s relevant experience in QA process for software development.
        //         Experience of SQL, and API testing.`,
        //     },
        //     {
        //         jobTitle:"Java Developer",
        //         location:"Chicago IL",
        //         salary:"Non payed",
        //         jobType:"fullTime/partTime/contract/internship/training",
        //         jobDescription:`In this role, you will apply Agile and SDLC Quality Assurance (QA) methodology and standards to verify and validate products meet specifications and customer acceptance criteria. `,
        //         responsibilities:`Use complex SQL queries for testing backend data.`,
        //         qualifications:`Relevant degree preferred in Computer Science.`,
        //     },
        //     {
        //         jobTitle:"Java Developer",
        //         location:"Chicago IL",
        //         salary:"Non payed",
        //         jobType:"fullTime/partTime/contract/internship/training",
        //         jobDescription:`In this role, you will apply Agile and SDLC Quality Assurance (QA) methodology and standards to verify and validate products meet specifications and customer acceptance criteria. `,
        //         responsibilities:`Use complex SQL queries for testing backend data.`,
        //         qualifications:`Relevant degree preferred in Computer Science.`,
        //     },
        //     {
        //         jobTitle:"Java Developer",
        //         location:"Chicago IL",
        //         salary:"Non payed",
        //         jobType:"fullTime/partTime/contract/internship/training",
        //         jobDescription:`In this role, you will apply Agile and SDLC Quality Assurance (QA) methodology and standards to verify and validate products meet specifications and customer acceptance criteria. `,
        //         responsibilities:`Use complex SQL queries for testing backend data.`,
        //         qualifications:`Relevant degree preferred in Computer Science.`,
        //     },
        //     {
        //         jobTitle:"Java Developer",
        //         location:"Chicago IL",
        //         salary:"Non payed",
        //         jobType:"fullTime/partTime/contract/internship/training",
        //         jobDescription:`In this role, you will apply Agile and SDLC Quality Assurance (QA) methodology and standards to verify and validate products meet specifications and customer acceptance criteria. `,
        //         responsibilities:`Use complex SQL queries for testing backend data.`,
        //         qualifications:`Relevant degree preferred in Computer Science.`,
        //     },
        // ];


       

       
        return (
        <div>
            <div className="CarrersPageBgImg pageBaseComponent mainPageFirstLvl w-100 mx-1 px-5  ">
            </div>
            <div className="mainPageSecondLvl  w-100 mx-1 px-5 textCenter bg-dark servicePageCaptionText">
                <hr className="solid my-0"></hr>
                    <br/>
                    <h2>OPEN POSITIONS</h2>
                    <br/>
                <hr className="solid my-0"></hr>
            </div>
            <div className="mainPageFourthLvl  w-100 mx-1 py-2 px-2 ">
                {/* ///////////////////////////////////////////////////////////// */}
                <Accordion defaultActiveKey="0">
                    <Card className='px-0'>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                        {/* /////////////////////////////////////////////////////////// */}
                        <Card className="text-center mx-0 my-0">
                            <Card.Header><h1>MERN Stack developer</h1></Card.Header>
                            <Card.Body>
                                <Card.Title>Non Paid training</Card.Title>
                                <Card.Text>
                                Basic knowladge of JS, HTML, CSS are required<br/>
                                Chicago, IL 60606
                                </Card.Text>
                                <Button onClick={()=>this.applyJob("MERN Stack developer")} variant="primary">Apply</Button>
                               {/* ////////////////////////////////////////////// */}
                               
                               
                               {/* ////////////////////////////////////////////// */}
                            </Card.Body>
                            <Card.Footer className="text-muted">2 days ago</Card.Footer>
                        </Card>
                        {/* /////////////////////////////////////////////////////////// */}
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0" className="textJustify">
                        <Card.Body>
                        <h3 className="textCenter">Full Job Description</h3>
If you are an engineer who's passionate about building impactful products that scale to tens of millions of page views a day, Indeed is looking for you. Indeed offers skilled developers like you a complex development ecosystem with short release cycles. Every week sees the new release of multiple products that meet the needs of jobseekers worldwide.
<br/>
Responsibilities:
<ul>
    <li>
Write software to fulfill well-specified, small-scoped work requests for your team's product(s), escalating more complex or ambiguous issues to senior engineers.
    </li>
    <li>
Write software to fix simple bugs.
    </li>
    <li>
Assist in investigating problems and recommending fixes for products within your team.
    </li>
    <li>
Utilize knowledge of programming languages and the software ecosystem to accomplish goals.
    </li>
    <li>
Provide feedback within your team regarding code changes made by other engineers and regarding designs for new enhancements.
    </li>
    <li>
Produce documentation for your team’s products, code, and work items. Grow your skills and knowledge learning from mentors, reading books or online resources, or by observing your co-workers applying their skills and knowledge.   
    </li>
</ul>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="1">
                        {/* /////////////////////////////////////////////////////////// */}
                         {/* /////////////////////////////////////////////////////////// */}
                         <Card className="text-center">
                            <Card.Header><h1>JAVA developer</h1></Card.Header>
                            <Card.Body>
                                <Card.Title>Non Paid training</Card.Title>
                                <Card.Text>
                                Basic knowladge of JAVA, JS, HTML, CSS are required<br/>
                                Chicago, IL 60606
                                </Card.Text>
                                <Button onClick={()=>this.applyJob()} variant="primary">Apply</Button>
                            </Card.Body>
                            <Card.Footer className="text-muted">2 days ago</Card.Footer>
                        </Card>
                        {/* /////////////////////////////////////////////////////////// */}
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                        <Card.Body className="textJustify">
                        <h3 className="textCenter">Full Job Description</h3>
If you are an engineer who's passionate about building impactful products that scale to tens of millions of page views a day, Indeed is looking for you. Indeed offers skilled developers like you a complex development ecosystem with short release cycles. Every week sees the new release of multiple products that meet the needs of jobseekers worldwide.
<br/>
Responsibilities:
<ul>
    <li>
Write software to fulfill well-specified, small-scoped work requests for your team's product(s), escalating more complex or ambiguous issues to senior engineers.
    </li>
    <li>
Write software to fix simple bugs.
    </li>
    <li>
Assist in investigating problems and recommending fixes for products within your team.
    </li>
    <li>
Utilize knowledge of programming languages and the software ecosystem to accomplish goals.
    </li>
    <li>
Provide feedback within your team regarding code changes made by other engineers and regarding designs for new enhancements.
    </li>
    <li>
Produce documentation for your team’s products, code, and work items. Grow your skills and knowledge learning from mentors, reading books or online resources, or by observing your co-workers applying their skills and knowledge.   
    </li>
</ul>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
                {/* ///////////////////////////////////////////////////////////// */}
                </div>
            <MyVerticallyCenteredModal
                title = {this.state.jobTitle}
                show={this.state.modalShow}
                onHide={() => this.setModalShow(false)}
            />
        </div>
        );
    }
}


export default CareersListComponent;