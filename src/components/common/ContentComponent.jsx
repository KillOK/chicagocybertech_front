import React from 'react';
import {Switch, Route} from 'react-router-dom';
import MainPage from '../../pages/MainPage';
import ServicesListPage from '../../pages/ServicesListPage';
import OrderPage from '../../pages/OrderPage';
import ProfilePage from '../../pages/ProfilePage';
import ContactPage from '../../pages/ContactPage';
import CareersPage from '../../pages/CareersPage';

import {Container} from "react-bootstrap";
import ApplicationPage from '../../pages/mock/ApplicationPage';
import LoginPage from '../../pages/LoginPage';
import SignUpPage from '../../pages/SignUpPage';
import ChangePwdPage from '../../pages/ChangePwdPage';
import ChangePwdLinkSubmission from '../../pages/ChangePwdLinkSubmission';
import ConfirmationLinkSubmission from '../../pages/ConfirmationLinkSubmission';

let ContentComponent = (props) =>{

    return(
        <div>
            <Container className="container py-3">
                <Switch>
                    <Route exact path='/' component={MainPage}/>
                    <Route exact path='/main' component={MainPage}/>
                    <Route exact path='/services' component={ServicesListPage}/>
                    <Route exact path='/projects' component={OrderPage}/>
                    <Route exact path='/profile' component={ProfilePage}/>
                    <Route exact path='/contact' component={ContactPage}/>
                    <Route exact path='/careers' component={CareersPage}/>
                    <Route exact path='/application' component={ApplicationPage}/>
                    <Route exact path='/login' component={LoginPage}/>
                    <Route exact path='/signup' component={SignUpPage}/>
                    <Route exact path='/pwdchange' component={ChangePwdPage}/>
                    <Route exact path='/pwdchangelink/:id' component={ChangePwdLinkSubmission}/>
                    <Route exact path='/confirmationlink/:id' component={ConfirmationLinkSubmission}/>
                </Switch>
            </Container>

        </div>
    );

}
export default ContentComponent;

