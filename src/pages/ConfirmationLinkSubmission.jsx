import React from 'react';
import {Form, Button, Modal, Col} from 'react-bootstrap';
import {connect} from "react-redux";
import{login,logout} from '../store/actions/authentication/mockLoginAction';
import{confLinkAction} from '../store/actions/security/confirmationLinkAction';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCheck } from "@fortawesome/free-solid-svg-icons";
// ConfirmationLink Submission for Sign Up
let MyVerticallyCenteredModal=(props) =>{
    console.log(props)
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter" >
            {props.title?props.title:"Something went wrong, try to complete the process one more time"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            {props.title?"If Registration failed, try to Sign Up and generate change password link mail one more time else Try to Sign In ":"Something went wrong, try to complete the process one more time"}
           {/* Try to Sign In else try to generate change pwd link mail one more time */}
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

class ConfirmationLinkSubmission extends React.Component {
    state = {
        modalShow:true,
        minHeight:{
          minHeight: (window.innerHeight - 180)+ 'px',
        },
    };
    setModalShow = (val)=>{
      console.log(
         this.props.errMessage
        )
        this.setState(
            {
                ...this.state,
                modalShow:val
            }
        )
    }

    componentDidMount() {
      this.props.confirmationAction(this.props.match.params.id);

    }

render() {
  
  
    return (
      <div style={this.state.minHeight}>
        <div className="pageBaseComponent mainPageFirstLvl  w-100 mx-1 px-5 textCenter">
            <br/>
            <br/>
           <h1 style={{ fontSize: '600%' }}><FontAwesomeIcon icon={faUserCheck} /></h1>
            <br/>
            <br/>
        </div>
        <div className="mainPageSecondLvl  w-100 mx-1 px-5 textCenter">
            <br/>
            <h4>Link verification</h4>
            <br/>
        </div>
        <div className="mainPageThirdLvl  w-100 mx-1 px-5 ">
            <br/>
                <Form.Row>
                <Col lg="3"></Col>
                    <Form.Group as={Col} lg="6">
                    <Button style={{width:"100%"}} onClick={(e)=>{e.preventDefault(); this.setModalShow(true) }}>Show modal</Button>
                    </Form.Group>
                <Col lg="3"></Col>
                </Form.Row>
            <br/>
                <Form.Row>
                <Col lg="3"></Col>
                    <Form.Group as={Col} lg="6">
                    <Button style={{width:"100%"}} onClick={(e)=>{e.preventDefault(); this.props.history.push("/login") }}>Sign In</Button>
                    </Form.Group>
                <Col lg="3"></Col>
                </Form.Row>
            <br/>
        </div>
        <div className="mainPageSecondLvl  w-100 mx-1 px-5 ">
            <br/>
            <h4  className="textCenter">If your link expired try to sign up again or try change password</h4>
            <h6 className="textCenter my-0">Remember, you should enter email which was used to generate confirmation link</h6>
            <br/>
        </div>
        <div className="mainPageThirdLvl  w-100 mx-1 px-5 ">
            <br/>
            <Form.Row>
              <Col lg="3"></Col>
                <Form.Group as={Col} lg="6">
                  <Button variant="danger" style={{width:"100%"}} onClick={(e)=>{e.preventDefault(); this.props.history.push("/pwdchange") }}>Change Password</Button>
                </Form.Group>
              <Col lg="3"></Col>
            </Form.Row>
            <br/>
        </div>
        {/* ========================================================================================= */}
        
        <div className="mainPageFourthLvl  w-100 mx-1 px-5 " style={{background: "lightgray"}}>
            <br/>
        </div>
        <MyVerticallyCenteredModal
                title={this.props.errMessage}
                show={this.state.modalShow}
                onHide={() => this.setModalShow(false)}
            />
    </div>
      );
    }
  }

  const mapStateToProps = (state)=>{
    return{
      user:state.auth.user,
      validated:state.userRegistration.validated,
      errMessage:state.confirmationLinkFeedback.errMessage
    }
  }

  const mapDispatchToProps = (dispatch)=>{
    return{
      logout:()=>{dispatch(logout())},
      login:(user)=>{dispatch(login(user))},
      confirmationAction:async (val)=>{dispatch(confLinkAction(val))},
    }
  }  
  
export default connect(mapStateToProps,mapDispatchToProps)(ConfirmationLinkSubmission);