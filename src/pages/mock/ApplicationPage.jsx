import React from 'react';
import FormToTry from './FormToTry';


let ApplicationPage = (props) =>{
    return(
        <div >
            
           <div className="ApplyPageBgImg pageBaseComponent mainPageFirstLvl w-100 mx-1 px-5  ">
            </div>
            <div className="mainPageSecondLvl  w-100 mx-1 px-1 textCenter bg-dark servicePageCaptionText">
                <hr className="solid my-0"></hr>
                    <br/>
                    <div className="px-3">
                        <h2>Form your application, press the print button and send it to our email </h2>
                    </div>
                    <h4>apply@chicagocybertech.com</h4>
                    <br/>
                <hr className="solid my-0"></hr>
            </div>
            <div className="mainPageFourthLvl  w-100 mx-1 px-5 ">
                <hr className="solid my-0"></hr>
                <br/>
                    <FormToTry validated={false} {...props}/> 
                <br/>
                
                <hr className="solid my-0"></hr>
                </div>
        </div>
    );

}
export default ApplicationPage;