import React from 'react';
import {Form,InputGroup,Button, Col} from 'react-bootstrap';

import {connect} from "react-redux";
import{login,logout} from '../../store/actions/authentication/mockLoginAction';
import{testvalidation} from '../../store/actions/authentication/formValidationActions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid} from '@fortawesome/fontawesome-svg-core/import.macro'
class FormToTry extends React.Component {


  
    handleSubmit(event) {
      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        this.props.testvalidate({ validated: true });
      }else{
        event.preventDefault();
        event.stopPropagation();
        var user = {};
        user.loggedIn=true;
        user.fname = event.currentTarget.validationCustom01.value;
        user.lname = event.currentTarget.validationCustom02.value;
        user.username = event.currentTarget.validationCustomUsername.value;
        this.props.login(user);
        this.props.history.push('/');
        this.props.testvalidate({ validated: false });
    }
    event.preventDefault();
    event.stopPropagation();
}

render() {
  const {validated} = this.props.validated;
  let printPage = (e)=>{
    e.preventDefault();
    window.print();return false;
}
    return (
        <div>
          <br/>
          <br/>
        <Form
          noValidate
          validated={validated}
          onSubmit={e => this.handleSubmit(e)}
        >
          <Form.Row>
            <Form.Group as={Col} md="4" controlId="validationCustom01">
              <Form.Label>First name</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="First name"
                defaultValue={this.props.user.lname?this.props.user.lname:''}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom02">
                <Form.Label>Last name</Form.Label>
                <Form.Control
                  required
                  type="text"
                  placeholder="Last name"
                  defaultValue=""
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">
                  Please provide your last name.
                </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustomUsername">
              <Form.Label>e-mail</Form.Label>
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  type="text"
                  placeholder="e-mail"
                  aria-describedby="inputGroupPrepend"
                  required
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">
                  Please enter e-mail.
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} md="6" controlId="validationCustom03">
              <Form.Label>City</Form.Label>
              <Form.Control type="text" placeholder="City (Optional)" required />
              <Form.Control.Feedback type="invalid">
                Please provide a valid city.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="3" controlId="validationCustom04">
              <Form.Label>State</Form.Label>
              <Form.Control type="text" placeholder="State (Optional)" required />
              <Form.Control.Feedback type="invalid">
                Please provide a valid state.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="3" controlId="validationCustom05">
              <Form.Label>Zip</Form.Label>
              <Form.Control type="text" placeholder="Zip (Optional)" required />
              <Form.Control.Feedback type="invalid">
                Please provide a valid zip.
              </Form.Control.Feedback>
            </Form.Group>
          </Form.Row>
          <Form.Group>
            <Form.Label>Phone</Form.Label>
            <Form.Control
              type="text"
              id="inputPhone"
              aria-describedby="inputPhoneBlock"
              placeholder="Phone"
            />
          </Form.Group>
          <div className="form-group ">
            <label >Please describe your project or application purpose</label>
            <textarea className="form-control inputPurpose" id="exampleFormControlTextarea5" rows="19"></textarea>
          </div>
          <Form.Group className='textCenter'>
            <Button className='mx-auto'  type="submit" onClick={printPage }><span> &nbsp;&nbsp;Print &nbsp;&nbsp;<FontAwesomeIcon icon={solid('print')} /></span></Button>
          </Form.Group>
        </Form>
        </div>
      );
    }
  }
  
  const mapStateToProps = (state)=>{
    return{
      user:state.auth.user,
      validated:state.testvalid.testValidation,
    }
  }

  const mapDispatchToProps = (dispatch)=>{
    return{
      logout:()=>{dispatch(logout())},
      login:(user)=>{dispatch(login(user))},
      testvalidate:(val)=>{dispatch(testvalidation(val))}
    }
  }  
  

export default connect(mapStateToProps,mapDispatchToProps)(FormToTry);