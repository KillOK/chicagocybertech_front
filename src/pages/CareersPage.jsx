import React from 'react';

import {connect} from 'react-redux';
import CareersListComponent from '../components/CareersListComponent/CareersListComponent';


class CareersPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }

    componentDidMount(){
    }
    render(){

    return(
        <div>
            <CareersListComponent />
        </div>
    );
    }

    
}

const mapStateToProps = (state) => {
    return {
    }
}
export default connect(mapStateToProps)(CareersPage);