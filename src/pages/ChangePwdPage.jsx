import React from 'react';

import {Button,} from 'react-bootstrap';
import {Form,Modal, Col} from 'react-bootstrap';
import {connect} from "react-redux";
import{login,logout} from '../store/actions/authentication/mockLoginAction';
import{enterMailForPwdChangeAction, cleanRegistrationMsg, setModalShow, formvalidation} from '../store/actions/security/changePwdActions';

let MyVerticallyCenteredModal=(props) =>{
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter" >
            {props.title?props.title:"Password change link was sent to your email."}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4> Please follow the link to change your password</h4>
          <p>
           If you did not receive the email, check your spam folder or try one more time 30 seconds later.
           Thank you!
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

class ChangePwdPage extends React.Component {
    state = {
    };
    
    ////////////////////////////////////////////////
    handleSubmit(event) {
    event.preventDefault();
    event.stopPropagation();
      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        this.props.testvalidate({ validated: true });
        
    }else{
        event.preventDefault();
        event.stopPropagation();
        this.props.sendPwdChangeLink({email:event.currentTarget.validationEmail01.value});
        this.props.testvalidate({validated: false });
    }

}
handleChange (event){
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
      this.props.testvalidate({ validated: true });
    }else if (event.currentTarget.validationEmail01.value) {
      event.preventDefault();
      event.stopPropagation();
      event.currentTarget.validationEmail01.isInvalid=true
      this.props.testvalidate({ 
          validated: true 
      });
    }
  
}


render() {
  const {validated} = this.props.validated;
  
    return (
    <div style={this.state.minHeight}>
        <div className="pageBaseComponent mainPageFirstLvl  w-100 mx-1 px-5 ">
            <img src="/MainPage/ChicagoSkyline.jpg" alt="Chicago.logo" width="100%"/>
            <br/>
            <br/>
        </div>
        <div className="mainPageSecondLvl  w-100 mx-1 px-5 ">
            <br/>
            <h4  className="textCenter">Please provide e-mail connected to your account</h4>
            <br/>
        </div>
        <div className="mainPageThirdLvl  w-100 mx-1 px-5 ">
                
          <br/>
        <Form
          validated={validated}
          onSubmit={e => this.handleSubmit(e)}
        >
          <Form.Row>
            <Col lg="3"></Col>
            <Form.Group as={Col} lg="6" controlId="validationEmail01">
              <Form.Control
                required
                type="text"
                className="textCenter"
                placeholder="e-mail"
                defaultValue={this.props.user.email?this.props.user.email:''}
                />
                <Form.Control.Feedback className="textCenter">Looks good!</Form.Control.Feedback>
                <Form.Control.Feedback className="textCenter" type="invalid">
                    Please provide your e-mail.
                </Form.Control.Feedback>
            </Form.Group>
            <Col lg="3"></Col>
            </Form.Row>
          <Form.Row>
            <Col lg="3"></Col>
            <Form.Group as={Col} lg="6" controlId="validationBtn">
              <Button style={{width:"100%"}} type="submit">Change password</Button>
            </Form.Group>
            <Col lg="3"></Col>
          </Form.Row>
        </Form>
        </div>
        <div className="mainPageSecondLvl  w-100 mx-1 px-5 ">
            <br/>
            <h4  className="textCenter">If you remember your password please try to Sign In</h4>
            <br/>
        </div>
        <div className="mainPageThirdLvl  w-100 mx-1 px-5 ">
            <br/>
            <Form.Row>
                <Col lg="3"></Col>
                    <Form.Group as={Col} lg="6">
                    <Button style={{width:"100%"}} onClick={(e)=>{e.preventDefault(); this.props.history.push("/login") }}>Sign In</Button>
                    </Form.Group>
                <Col lg="3"></Col>
            </Form.Row>
            <br/>
        </div>
        {/* ========================================================================================= */}
        
        <div className="mainPageFourthLvl  w-100 mx-1 px-5 " style={{background: "lightgray"}}>
            <br/>
        </div>
        <MyVerticallyCenteredModal
                show={this.props.modalShow}
                onHide={async () => {
                  await this.props.cleanRegistrationMsg();
                  this.props.setModalShow(false);
                  this.props.history.push('/');
              }}
              title= {this.props.registrationmsg!==""?this.props.registrationmsg:"Something went wrong, try one more time 30 seconds later"}
            />
    </div>
      );
    }
  }

  const mapStateToProps = (state)=>{
    return{
      user:state.auth.user,
      validated:state.testvalid.testValidation,
      registrationmsg:state.userRegistration.registrationMessage,
      modalShow:state.userRegistration.modalShow,
    }
  }

  const mapDispatchToProps = (dispatch)=>{
    return{
      logout:()=>{dispatch(logout())},
      login:(user)=>{dispatch(login(user))},
      sendPwdChangeLink:(val)=>{dispatch(enterMailForPwdChangeAction(val))},
      testvalidate:(val)=>{dispatch(formvalidation(val))},
      setModalShow:async (val)=>{dispatch(setModalShow(val))},
      cleanRegistrationMsg:()=>{dispatch(cleanRegistrationMsg())}
    }
  }  

export default connect(mapStateToProps,mapDispatchToProps)(ChangePwdPage);