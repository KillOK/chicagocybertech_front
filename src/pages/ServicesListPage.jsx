import React from 'react';
import {connect} from 'react-redux';
import ServicesListComponent from '../components/ServicesListComponent/ServicesListComponent';

class ServicesListPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }

    componentDidMount(){
    }
    render(){
    return(
        
        <div >
            <ServicesListComponent />
        </div>
    );
    }

    
}

const mapStateToProps = (state) => {
    return {
    }
}
export default connect(mapStateToProps)(ServicesListPage);