const initState = {
    errMessage:"",
}



export const confLinkReducer = (state=initState,action) => {
    if(action.type==='confLinkAction'){
        return{
            ...state,
            errMessage:action.message
        }
    }
    else if(action.type==='confLinkMsg'){
        return{
            ...state,
            errMessage:""
        }
    }
    else return {...state};
}

export default confLinkReducer;