import authReducer from './authentication/authReducer';
import testFormValid from './authentication/formValidationReducers';
import loginValidationReducer from './authentication/loginValidationReducer';
import registrationReducer from './security/registrationReducer';
import confirmationLinkReducer from './security/confirmationLinkReducer';
import changePwdReducer from './security/changePwdReducer';
import pwdChangeLinkReducer from './security/pwdChangeLinkReducer';
import {combineReducers} from "redux";

const rootReducer = combineReducers({
    auth:authReducer,
    testvalid:testFormValid,
    loginvalid:loginValidationReducer,
    userRegistration:registrationReducer,
    confirmationLinkFeedback:confirmationLinkReducer,
    changePwdReducer:changePwdReducer,
    pwdChangeLinkReducer:pwdChangeLinkReducer,
})


export default rootReducer;