const initState = {
    testValidation: {validated:false,},
    samePwd:{ validated: false  },
    }
    
    const loginValidationReducer = (state=initState,action) => {
        // console.log("action.samePwd");
        // console.log(action.samePwd);
            if(action.type==='loginvalidation'){
                return{
                    ...state,
                    testValidation:{ validated: action.testValidation  },
                    samePwd:{ validated: action.samePwd  }
                }
            }
        else return {...state};
    }
    
    export default loginValidationReducer;