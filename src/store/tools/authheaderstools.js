
import {setCookie, getCookie} from '../tools/cookieManagementTools';
import axios from 'axios';

//{token:token, expTime:expTime}
// after loginor app start, triger the function which will refresh auth cookie before exp time with setInterval... -> fetch token from cookies
export const authorizedAxiosTool = async() => {
    // check session cookie if token, apply header:else return axios
    let sessionCookie = getCookie("sessionCookie");
   //console.log("=========authorizedAxiosTool.getCookie=============");
   //console.log(sessionCookie);
    ////console.log("Using authorized xhr tool")
    if(sessionCookie)axios.defaults.headers.common['Authorization'] = `Bearer_${sessionCookie}`;
   //console.log(axios.defaults.headers.common);
    return axios;
}

// run after app start and after login
export const refreshAuthToken = (params) => {
        let domain = "localhost:4000";
        let sessionCookie = getCookie("sessionCookie");
        let expiresInSeconds = getCookie("expiresInSeconds");
        let userEmail = getCookie("user_email");
        let userId = getCookie("userId");
       //console.log("cookie===============sessionCookie===============");
       //console.log(sessionCookie);
        let refreshLogic = async function () { 
           //console.log("Trying to apply new Authorization header");
            sessionCookie = getCookie("sessionCookie");
            expiresInSeconds = getCookie("expiresInSeconds");
            userEmail = getCookie("user_email");
            userId = getCookie("userId");
           //console.log("===============sessionCookie===============");
           //console.log(sessionCookie);
            //refresh token if required
            if(sessionCookie){
                // if(expiresInSeconds>((new Date().getTime() + 7 * 24 * 60 * 60 * 1000 )/1000)){  //7d
                //     axios.defaults.headers.common['Authorization'] = `Bearer ${cookie.token}`;
                // }
                // else 
                if(expiresInSeconds>(new Date().getTime()/1000)){
                   //console.log("expiresInSeconds");
                   //console.log(expiresInSeconds);
                    ////console.log(axios.defaults.headers.common);
                    // fetch new refresh token, store new sessionCookie
                    // let axios.defaults.headers.common['Authorization'] = `Bearer_${sessionCookie}`;
                    let axios = await authorizedAxiosTool();
                    // axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
                    ////console.log(axios.defaults.headers.common);
                    try {
                        let resp = await axios
                        .post('http://'+domain+'/refreshToken',{
                            email:userEmail,
                            userId:userId
                            ,
                            deviceType:sessionCookie.deviceType,
                            browserName:sessionCookie.browserName,
                            platformName:sessionCookie.platformName
                        });
                        // let tkn = resp.data.data.token?resp.data.data.token:sessionCookie;
                        ////console.log("resp");
                        ////console.log(resp);
                        ////console.log(sessionCookie);
                        if(resp.data.data.token){
                           //console.log("------------------------------------");
                           //console.log("-----------Store cookie-------------");
                           //console.log("------------------------------------");
                            setCookie("sessionCookie",resp.data.data.token,30);
                            axios.defaults.headers.common['Authorization'] = `Bearer_${resp.data.data.token}`;
                        }
                    } catch (error) {
                       console.log("Was not able to refresh session");
                       console.log(error);
                    }
                }
                else{
                    // delete sessionCookie
                   console.log(expiresInSeconds);
                   console.log(new Date().getTime()/1000);
                   console.log("Delete sessionCookie due to expiration date");
                    setCookie("sessionCookie","",NaN);
                }
            }
        };
        refreshLogic();
        setInterval(refreshLogic,
            // (30*1000));
        (12*60*60*1000));
}
