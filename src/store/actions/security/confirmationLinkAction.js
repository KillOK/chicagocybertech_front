import axios from 'axios';

let domain = "localhost:4000";
export const confLinkAction=(val)=>{
    return async(dispatch, getState)=>{
        axios
        .get('http://'+domain+'/confirmation/'+val)
        .then(async response => {
            await dispatch({type:"confLinkAction", message:response.data.message?response.data.message:"Something went wrong, try to sign up again"});
        })
        .catch(err=>{
            console.log({err:err});
        });
    }
}

export const cleanRegistrationMsg = () => {
    console.log("cleanRegistrationMsg")
    return {type:'cleanRegistrationMsg',
    message:""}
}
