
import axios from 'axios';
let md5 = require('md5');

let domain = "localhost:4000";
export const PwdChangeLinkAction=(param)=>{
    return async(dispatch, getState)=>{
        let pwd = await md5(param.password);
        console.log(pwd);
        axios
        .post('http://'+domain+'/changepassword/'+param.link,{
            email:param.login,
	        password:pwd
        })//+param
        .then(async response => {
            await dispatch({type:"PwdChangeLinkAction", message:response.data.message?response.data.message:response.data});
            console.log(response.data);
        })
        .catch(err=>{
            console.log({err:err,param:param});
        });
        
    }
}

export const setModalShow = (val) => {
    console.log("setmodalshow "+val)
    return {type:'setModalShow',
    modalShow:val}
}

export const cleanRegistrationMsg = () => {
    console.log("cleanRegistrationMsg")
    return {type:'cleanRegistrationMsg',
    message:""}
}

export const formvalidation=(val)=>{
    return { 
        type:'formValidation',
        validation:val.validated ,
        samePwd:val.samePwd,
        fillForm:val.fillForm
    }
 }

