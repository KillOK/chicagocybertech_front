
import {authorizedAxiosTool} from '../../tools/authheaderstools';
let axios = authorizedAxiosTool();

let domain = "localhost:4000";
export const enterMailForPwdChangeAction=(param)=>{
    return async(dispatch, getState)=>{
        axios
        .post('http://'+domain+'/forgotpassword',{
            email:param.email
          })
        .then(async response => {
            await dispatch({type:"SignUpAction", message:response.data.message?response.data.message:response.data});
            console.log("action.message");
        })
        .catch(err=>{
            console.log({err:err});
        });
        
    }
}

export const setModalShow = (val) => {
    console.log("setmodalshow "+val)
    return {type:'setModalShow',
    modalShow:val}
}

export const cleanRegistrationMsg = () => {
    console.log("cleanRegistrationMsg")
    return {type:'cleanRegistrationMsg',
    message:""}
}

export const formvalidation=(val)=>{
    return { 
        type:'formValidation',
        validation:val.validated 
    }
 }

