
import axios from 'axios';
import {setCookie, getCookie} from '../../tools/cookieManagementTools';

let md5 = require('md5');



export const initalLogin=()=>{
  console.log("INITAL_LOGIN");
  let usr = getCookie("user_email");
  let userId = getCookie("userId");
  console.log("sessioncookie from initial login");
  console.log(getCookie("sessionCookie"));
  return { 
    type:'INITAL_LOGIN',
    user:usr?{
      loggedIn:true,
      userId: userId?userId:"",
      email: usr
    }:{ loggedIn:false},
    token:getCookie("sessionCookie"),
    expiresInSeconds:getCookie("expiresInSeconds")?getCookie("expiresInSeconds"):((new Date().getTime() + 29 * 24 * 60 * 60 * 1000 )/1000)
  }
}

export const logout=()=>{
  // Add token revokation on backend
  setCookie("user_email", "", 30);
  setCookie("userId", "", 30);
  setCookie("sessionCookie", "", 30)
  return { 
      type:'LOGOUT',
      user:{ loggedIn:false},
      token:"",
    }
  }
  
  let domain = "localhost:4000";
  
  export const login = (loginData, myFunc)=>{
    let user = loginData.user;
    return async(dispatch, getState)=>{
      let pwd = await md5(user.password);
        axios
        .post('http://'+domain+'/login',{
          email:user.email,
	        password:pwd,
          deviceType:loginData.deviceType,
          browserName:loginData.browserName,
          platformName:loginData.platformName
        })
        .then(async response => {
          await dispatch(
                {
                    type:"LOGIN", 
                    user:{ 
                        loggedIn:true,
                        userId: response.data?response.data.userId:"No Data",
                        email: response.data?response.data.email:"No Data"
                    },
                    token:response.data.token,
                    expiresInSeconds:response.data?response.data.expiresInSeconds:2000
                }
            );
            // store session cookie
            return {
              expiresInSeconds:response.data?response.data.expiresInSeconds:2000,
              user:{ 
                loggedIn:true,
                userId: response.data?response.data.userId:"No Data",
                email: response.data?response.data.email:"No Data",
                token:response.data.token?response.data.token:"No Data",
              }
              }
        })
        .then(cookies=>{
            setCookie("user_email", cookies.user.email, 30);
            setCookie("userId", cookies.user.userId, 30);
            setCookie("sessionCookie", cookies.user.token, 30);
            setCookie("expiresInSeconds", cookies.expiresInSeconds, 30);
            return "Saved sessioncookie with expTime"
          })
          .then(result=>{
            loginData.props.history.push('/'); // go to main page after login
        })
        .catch(async err=>{
            console.log(err);
            await dispatch(
                {
                    type:"LOGIN_ERR", 
                    user:{ 
                        loggedIn:false
                    },
                    loginerr:true,
                    loginerrmsg:err.response?err.response.data.message:"Please enter correct data"
                },
                    
            );
            loginData.props.setLoginModalShow(true);
            
        }); 
          
    }
    
}


export const setModalShow = (val) => {
    return {type:'setloginModalShow',
    modalShow:val}
}

export const cleanLoginMsg = () => {
    return {type:'cleanLoginMsg',
    message:""}
}

