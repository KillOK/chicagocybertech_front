import React, {useEffect} from 'react';
import HeaderComponent from './components/common/HeaderComponent';
import ContentComponent from './components/common/ContentComponent';
import FooterComponent from './components/common/FooterComponent';
import '../src/styles/style.css';
import {connect} from "react-redux";
import{login,logout} from './store/actions/authentication/mockLoginAction';
import{refreshAuthToken} from './store/tools/authheaderstools';
import {Button} from 'react-bootstrap'

function App(props) {
  useEffect(()=>{
        // do it on start to data from cookies 
        // and check if we should refresh our token
        refreshAuthToken();
    }, []) 

  const scrollToTop = () =>{
    window.scrollTo({
      top: 0, 
      behavior: 'smooth'
    });
  };

  return (

    <div className="App">
      <HeaderComponent ></HeaderComponent>
      <ContentComponent></ContentComponent>
      <FooterComponent></FooterComponent>
      <Button className='scrollUp ' variant="dark" size="md" onClick={scrollToTop}>&uarr;</Button>
    </div>
  );
}

const mapStateToProps = (state)=>{
  return{
    user:state.auth.user,
    token:state.auth.token,
  }
};

const mapDispatchToProps = (dispatch)=>{
  return{
    login:(user)=>{dispatch(login(user))},
    logout:()=>{dispatch(logout())},
  }
} ;


export default connect(mapStateToProps,mapDispatchToProps)(App);
